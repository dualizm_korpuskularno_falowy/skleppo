package controllers;

import models.User;

import java.io.IOException;
import java.util.Scanner;

public class UsersController extends AppController {

    public boolean start(){
        System.out.println("Menu uzytkownikow: ");
        System.out.println("Wybierz z menu: ");
        System.out.println("1. Dodaj");
        System.out.println("2. Usuń");
        System.out.println("3. Wyszukaj");
        System.out.println("4. Lista");
        Scanner sc = new Scanner(System.in);
        int input = sc.nextInt();

        switch (input){
            case 1:
                add();
            case 2:
//                AppController controller = new UsersController();
                break;
            case 3:
//                AppController controller = new UsersController();
                break;
            default:
                break;
        }

        return false;
    }

    public void add(){
        Scanner sc = new Scanner(System.in);
        User user = new User();
        System.out.println("Podaj imię");
        user.setFirstName(sc.nextLine());

        System.out.println("Podaj nazwisko");
        user.setLastName(sc.nextLine());

        System.out.println("Podaj Balans");
        user.setBalance(sc.nextDouble());

        try {
            user.save();
        }catch (IOException e){
            System.out.println("Coś poszło nie tak...");
            return;
        }
        System.out.println("Gratulacje użytkownik został dodany");
    }
}
