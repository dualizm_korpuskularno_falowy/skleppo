package models;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class User extends AppModel {
    private String modelName = "User"; /* @TODO potrzebne? XD */
    private String useTable = "users";

    private String firstName;
    private String lastName;
    private double balance;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public boolean save() throws IOException {
        String filePath = new File("").getAbsolutePath();


        FileWriter writer = new FileWriter("C:\\Users\\Damian\\IdeaProjects\\projekt sklep PO\\src\\db\\users.csv", true);
        writer.append(firstName);
        writer.append(",");
        writer.append(lastName);
        writer.append(',');
        writer.append(Double.toString(balance));
        writer.append(",");
        writer.append("\n");
        writer.flush();
        writer.close();
        return true;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", balance=" + balance +
                '}';
    }
}
